run:
docker-compose build

to setup php/apache/phpmyadmin

place all project files to /src

run docker:
docker-compose up

or silent:

docker-compose up -d

www root will be available at:

hf.local

phpmyadmin url is:
db.hf.local

see docker-compose.yml file for edits in config